#coding: utf-8 -*-

#export PYTHONPATH=$pythonpath:"."

class calculatorInt():
    """
    c'est un classe qui permet de calculer

    """
    @staticmethod
    def sum(a,b):
        """
        Input   : 2 integers a and b

        Output  : a+b if a and b are integer
                : error else
        """
        ret = "Error : not integer"
        if isinstance(a, int) & isinstance(b, int):
            ret = a+b
        return ret
    
    @staticmethod
    def sub(a,b):
        """
        Input   : 2 integers a and b

        Output  : a-b if a and b are integer
                : error else
        """
        ret = "Error : not integer"
        if isinstance(a, int) & isinstance(b, int):
            ret = a-b
        return ret

    @staticmethod
    def mul(a,b):
        """
        Input   : 2 integers a and b

        Output  : a*b if a and b are integer
                : error else
        """
        ret = "Error : not integer"
        if isinstance(a, int) & isinstance(b, int):
            ret = a*b
        return ret

    @staticmethod
    def div(a,b):
        """
        Input   : 2 integers a and b

        Output  : a+b if a and b are integer and b!=0
                : cannoit divide by 0 if b=0
                : error else
        """
        ret = "Error : not integer"
        if b==0:
            ret = "Cannot divide by zero"
            #raise ZeroDivisionError("Cannot divide by zero")
        elif isinstance(a, int) & isinstance(b, int):
            ret = a/b
        return ret

