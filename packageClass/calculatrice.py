#coding: utf-8 -*-

class calculator():
    """
    c'est un classe qui permet de calculer

    """
    @staticmethod
    def sum(a,b):
        """
        Input : 2numbers a and b

        Output : a+b
        """
        return a+b
    @staticmethod
    def sub(a,b):
        return a-b

    @staticmethod
    def mul(a,b):
        return a*b

    @staticmethod
    def div(a,b):
        return a/b

