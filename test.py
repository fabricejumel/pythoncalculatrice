#coding: utf-8 -*-
from packageClass.calculatrice import calculator
from packageClass.calculatriceInt import calculatorInt
import unittest
import logging

class TestStringMethods(unittest.TestCase):
    """
    ceci permet de tester les différentes fonctions et de voir les éventuels problemes
    sans pour autant stopper
    """
    def setUp(self):
        self.calculator = calculatorInt()

    def test_add(self):
        result = self.calculator.sum(5,6)
        self.assertEqual(result,11)
        self.assertNotEqual(result,12)
        logging.info("opération effectuée")
        logging.info("5+6=%d",result)

    def test_sub(self):
        result = self.calculator.sub(5,6)
        self.assertEqual(result,-1)
        self.assertNotEqual(result,1)
        logging.info("opération effectuée")
        logging.info("5-6=%d",result)

    def test_mul(self):
        result = self.calculator.mul(5,6)
        self.assertEqual(result,30)
        self.assertNotEqual(result,-30)
        logging.info("opération effectuée")
        logging.info("5*6=%d",result)

    def test_div(self):
        result = self.calculator.div(36,6)
        self.assertEqual(result,6)
        self.assertNotEqual(result,-6)
        logging.info("opération effectuée")
        logging.info("36/6=%d",result)

    def test_mul_reel(self):
        result = self.calculator.mul(5.0,6.0)
        self.assertEqual(result,"Error : not integer")
        self.assertNotEqual(result,30)
        logging.info("opération effectuée")
        
    def test_div0(self):
        result = self.calculator.div(5,0)
        self.assertNotEqual(result,0)
        logging.info("opération effectuée")
        logging.info(result)

#Tests :
a=5
b=7
s=calculator()
print("Test 1ere calculatrice : ")
print(s.sum(a,b))
print(s.sub(a,b))
print(s.mul(a,b))
print(s.div(a,b))
print("\n")

#Tests :
s2=calculatorInt()
print("Test 2eme calculatrice : ")
print(s2.sum(a,b))
print(s2.sub(a,b))
print(s2.mul(a,b))
print(s2.div(a,b))
print("\n")

print("Test erreurs de la 2eme calculatrice : ")
print(s2.mul(a,2.3))
print(s2.div(a,0))
print("\n")
if __name__ == '__main__':
    logging.basicConfig(filename='test.log', level=logging.INFO)
    logging.info('On commence le test')
    unittest.main()
    logging.info('test terminé')

#export PYTHONPATH=$pythonpath:"."
