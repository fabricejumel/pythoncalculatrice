import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="calculatrice",
    version="0.0.1",
    author="tom kauffmann",
    author_email="email@email.email",
    description="Ce fichier se lance avec le markdown",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/tom-test1/pythoncalculatrice",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
